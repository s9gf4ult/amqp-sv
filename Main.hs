module Main where

import Control.Applicative
import Control.Concurrent
import Network.AMQP

qname = "echo"

qhandler con msg envelope = go *> return ()
  where
    go = forkIO $ do
        putStrLn "received"
        case msgReplyTo msg of
            Nothing -> return ()
            Just resp -> do
                let chan = envChannel envelope
                publishMsg chan "" resp msg { msgReplyTo = Nothing }
                putStrLn "responsed"
        ackEnv envelope


main = do
    con <- openConnection "127.0.0.1" "/" "guest" "guest"
    putStrLn "Connected!"
    chan <- openChannel con
    _ <- declareQueue chan newQueue
                          { queueName = qname }

    recoverMsgs chan True
    consumeMsgs chan qname Ack (uncurry $ qhandler con)

    getLine
    closeConnection con
    putStrLn "Disconnected"
